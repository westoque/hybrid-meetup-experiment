class RoomsController < ApplicationController
  allow_unauthenticated only: :show
  skip_authorization only: [:new, :create]

  def new
  end

  def create
    redirect_to room_path(SecureRandom.uuid)
  end

  def show
    @client = Client.new(id: SecureRandom.uuid)
    cookies.encrypted[:client_id] = @client.id
    @room = Room.new(id: params[:id])
  end

private

  def authorizable_resource
    @listing = Listing.find(params[:id])
  end
end
