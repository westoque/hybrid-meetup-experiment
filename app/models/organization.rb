class Organization < ApplicationRecord
  validates :name, presence: true
  
  scope :ordered, -> { order(id: :desc) }

  has_many :memberships, dependent: :destroy
  has_many :members, through: :memberships, source: :user

end
