class User < ApplicationRecord
	rolify
	include Authentication

	before_validation :strip_extraneous_spaces

	has_one :membership, dependent: :destroy
	has_one :organization, through: :membership

	validates :name,
		presence: true

	validates :email,
		format: { with: URI::MailTo::EMAIL_REGEXP },
		uniqueness: { case_sensitive: false }

	rolify

private
	def strip_extraneous_spaces
		self.name = self.name&.strip
		self.email = self.email&.strip
	end
end
