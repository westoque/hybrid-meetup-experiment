require 'factory_bot_rails'

include FactoryBot::Syntax::Methods

org = create(:organization, name: 'PhRUG')
user = create(:user, email: 'superman@example.com', password: 'awesomest')
user.add_role(:admin)