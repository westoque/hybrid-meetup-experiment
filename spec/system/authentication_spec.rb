require 'rails_helper'

RSpec.describe "an unauthenticated user" do
	context "creating a user via email" do
		it "accepts registration via email & password" do 
			visit signup_path
			
			fill_in :user_name, with: "Lara"
			fill_in :user_email, with: "lara@kalarakan.net"
			fill_in :user_password, with: "blabbityblah"
			fill_in :user_password_confirmation, with: "blabbityblah"
			
			click_on "Sign up"
			
			new_user = User.last
			# expect(new_user.confirmed?).to be false
			# expect(ActionMailer::Base.deliveries.size).to eq 1
			
			# confirm_link = ActionMailer::Base.deliveries.last.encoded.match(/href="(?<url>.+?)">/)[:url]
			# visit confirm_link

			# new_user.reload

			# expect(new_user.email).to eq "this@email.here"
			# expect(new_user.confirmed?).to be true
			# expect(page).to have_text(:all, "Logout")
		end

	# 	it 'allows a user to reset their password' do 
	# 		user = create(:user)
			
	# 		visit login_path
	# 		click_on 'Forgot your password?'

	# 		fill_in :user_email, with: user.email
	# 		expect(ActionMailer::Base.deliveries.size).to eq 0
	# 		click_on "Send me reset password instructions"
	# 		expect(ActionMailer::Base.deliveries.size).to eq 1
			
	# 		token_link = ActionMailer::Base.deliveries.last.body.raw_source.match(/href="(?<url>.+?)">/)[:url]
	# 		visit token_link
	# 		fill_in :user_password, with: '2short'
	# 		fill_in :user_password_confirmation, with: '2short'
	# 		click_on 'Change my password'
	# 		expect(page).to have_text(I18n.t(".activerecord.errors.models.user.attributes.reset_password_token.too_short"))

	# 		fill_in :user_password, with: 'password#1'
	# 		fill_in :user_password_confirmation, with: 'password#2'
	# 		click_on 'Change my password'
	# 		expect(page).to have_text(I18n.t(".activerecord.errors.models.user.attributes.reset_password_token.doesnt_match"))

	# 		fill_in :user_password, with: 'newpassword'
	# 		fill_in :user_password_confirmation, with: 'newpassword'
	# 		click_on 'Change my password'

	# 		expect(page).to have_text(user.full_name)
	# 	end
	end

	context "logging in via email" do
		it "accepts an email & password" do 
			user = create(:user)

			visit login_path
			fill_in :user_email, with: user.email
			fill_in :user_password, with: user.password

			click_on('Log in')

			expect(page).to have_text(user.name)
		end
	end
end
