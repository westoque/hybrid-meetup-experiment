require 'rails_helper'
require_relative '../helpers/authentication_helpers'

RSpec.describe "Sessions", :type => :request do
  let(:user) { create(:user) }
  let(:new_user) { build(:user, email:"test@kibble.be", password: "youdontwannaknow") }

  def decrypt_session_cookie(request)
    jar = ActionDispatch::Cookies::CookieJar.build(request, cookies.to_hash)
    jar.encrypted["_booking_session"]
  end

  it "logs in an existing user" do
    user
    post "/login", :params => { user: {
        email: user.email, 
        password: user.password
      }
    } 

    expect(response).to redirect_to(root_path)
    follow_redirect!
    expect(response.body).to include I18n.t('sessions.create.success')

    check_for_user_id_in_session_cookie(user)
  end

  it "fails to login an existing user with wrong password" do
    user
    post "/login", :params => { user: {
        email: user.email, 
        password: 'gobbldeygook'
      }
    } 

    expect(response.body).to include I18n.t('sessions.create.incorrect_details')
    expect(response.status).to eq 422
  
    verify_no_user_id_in_session_cookie
  end

  it "logs out an existing user" do
    log_in user
    get root_url
    expect(response.body).to include(user.name)

    log_out
    follow_redirect!
    expect(user.app_sessions.count).to eq 0
    expect(response.body).to include(I18n.t(".sessions.new.title"))
    verify_no_user_id_in_session_cookie
  end
    
  it "denies access to a non-existent user" do
    post "/login", :params => { user: {
        email: new_user.email,
        password: new_user.password,
        sign_in_method: "email"
      }
    }

    expect(response.body).to include I18n.t('sessions.create.incorrect_details')
    expect(response.status).to eq 422
    verify_no_user_id_in_session_cookie
  end
end
