require 'rails_helper'

RSpec.describe "Registrations", :type => :request do
  describe "via email" do
    let(:user) { create(:user) }
    let(:new_user) { build(:user, email:"test@kibble.be", password: "youdontwannaknow") }
    it "rejects a signup with no input" do
      post "/signup", :params => { user: {
        	"email": "", 
        	"password": "", 
        	"password_confirmation": "",
          "signup_source": "email_signup"
        }
      } 

      expect(response.body).to include("password", "blank")
      expect(response.body).to include("user", "blank")
    end

    it "rejects a signup with no password too short" do
 	    post "/signup", :params => {  user: {
   	    	"user[email]": new_user.email, 
   	    	"user[password]": "sdfqwer", 
   	    	"user[password_confirmation]": "sdfqwer",
          "signup_source": "email_signup"  
   	    } 
      }

  		expect(response.body).to include("password", "short")
    end

    it "rejects a signup with mismatched passwords" do
   	    post "/signup", :params => { user: {
     	    	"user[email]": new_user.email, 
     	    	"user[password]": new_user.password, 
     	    	"user[password_confirmation]": new_user.password + "s",
            "signup_source": "email_signup"  
     	    }
        }
   	end

    it "rejects a signup with invalid email" do
 	    post "/signup", :params => { user: {
   	    	"user[email]": "testkibble.be", 
   	    	"user[password]": new_user.password, 
   	    	"user[password_confirmation]": new_user.password,
          "signup_source": "email_signup"  
   	    }
      }

  		expect(response.body).to include("email", "invalid")
    end

    it "rejects a signup for user with existing email" do
 	    post "/signup", :params => { user: {
   	    	"email": user.email, 
   	    	"password": new_user.password, 
   	    	"password_confirmation": new_user.password,
          "signup_source": "email_signup"  
   	    } 
      }

  		expect(response.body).to include("email", "already", "taken")
    end

    it "redirects a valid signup" do
 	    post "/signup", :params => { user: {
   	    	"name": new_user.name,
          "email": new_user.email, 
   	    	"password": new_user.password, 
   	    	"password_confirmation": new_user.password,
   	    }
      }

      expect(response).to redirect_to(root_path)
      follow_redirect!
  		# expect(response.body).to include("Logout")
    end
  end
end