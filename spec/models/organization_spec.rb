require 'rails_helper'

RSpec.describe Organization, type: :model do
  it "has a valid factory" do
    org = build(:organization, name: 'Holkien')
    
    expect(org).to be_valid
  end

  it "has many users" do
    org1 = create(:organization)
    org2 = create(:organization)

    user1 = create(:user)
    user2 = create(:user)
    user3 = create(:user)
    create(:membership, user: user1, organization: org1)
    create(:membership, user: user2, organization: org1)
    create(:membership, user: user3, organization: org2)

    expect(org1.members).to contain_exactly(user1, user2)
  end
end
