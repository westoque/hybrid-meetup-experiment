FactoryBot.define do
  factory :organization do
    name { CGI.escapeHTML(Faker::Company.unique.name) }
  end
end
