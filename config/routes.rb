Rails.application.routes.draw do
  get 'users/new'
  get 'users/create'
  root to: 'rooms#new'
  resources :rooms, only: [:new, :create, :show]

  get"signup",to: "users#new"
  post"signup",to: "users#create"

  get"login",to: "sessions#new"
  post"login",to: "sessions#create"

  delete "logout", to: "sessions#destroy"
end
